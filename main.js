// MyForm class
class MyForm {
  constructor (inputValue) {
    // Set the input value as a property of the class
    this.inputValue = inputValue
    // Bind event handlers
    this.onSubmit = this.onSubmit.bind(this)
    this.onToolbarClick = this.onToolbarClick.bind(this)
    // Get the input element
    this.input = document.querySelector('.form input')
  }

  onSubmit (event) {
    event.preventDefault()
    const inputValue = this.input.value
    inputValue === '' || !inputValue.trim()
      ? alert('Please enter the input field!')
      : alert(
          `Form submitted!  Input value: ${new MyForm(inputValue).inputValue}`
        )
  }

  onToolbarClick (event) {
    // Get the button that was clicked
    const button = event.target
    // Get the button's text content
    const buttonText = button.textContent
    alert(
      `${buttonText} clicked!,  Input value: ${
        this.inputValue === '' ? 'No input provided ' : this.inputValue
      }`
    )
  }
}

// Event listeners
document.querySelector('.form').addEventListener('submit', event => {
  const inputValue = document.querySelector('.form input').value
  new MyForm(inputValue).onSubmit(event)
})

document.querySelectorAll('.toolbar button').forEach(button => {
  button.addEventListener('click', event => {
    const inputValue = document.querySelector('.form input').value
    new MyForm(inputValue).onToolbarClick(event)
  })
})
